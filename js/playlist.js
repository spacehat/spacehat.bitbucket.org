var playlist = [
    {
        mp3: 'mix/song1.mp3',
        title: 'Blame It On Me',
        artist: 'Spacehat',
    },
    
    {
        mp3: 'mix/song2.mp3',
        title: 'Calamity Song',
        artist: 'Spacehat',
    },
    
    {
        mp3: 'mix/song3.mp3',
        title: 'Yoshimi Battles Pink Robots',
        artist: 'Spacehat',
    }
];